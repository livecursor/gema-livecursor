Gem::Specification.new do |s|
  s.name        = 'livecursor'
  s.version     = '0.0.2.1'
  s.executables << 'livecursor'
  s.date        = '2021-02-21'
  s.summary     = 'lv-h'
  s.description = 'Gema para hacer livecoding en la terminal'
  s.authors     = ['Carlos']
  s.email       = 'carsrouhs@gmail.com'
  s.files       = ['lib/livecurses.rb']
  s.homepage    = 'https://gitlab.com/livecursor'
  s.license = 'GPL-3.0'
  s.required_ruby_version = '>= 2.6.0'
  s.add_dependency 'curses', '~> 1.2'
end
